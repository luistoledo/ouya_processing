OUYA with Processing
====================


USB
---
- Got an issue with the adb USB, but fix with the 0x2836 tirck:
- Just follow the 'official' inscturctions
https://github.com/ouya/docs/blob/master/setup.md

Filters
-------
- not working (blur, dilate, etc)

3D
--
- working both P3D and OPENGL
- 

FPS
---
- 58 avg (but ive seen 78!)


Backbutton issue
----------------
issue:
- single press of A button quits the interface

solution:
- implements surfaceKeyDown_ and _surfaceKeyUp_. Return false when event key down is KEYCODE_BACK
- https://forum.processing.org/topic/override-the-behaviour-of-the-backbutton#25080000001208140


Fullscreen at lower res
-----------------------
- Add resolution support at manifest
  <supports-screens android:smallScreens="false" android:normalScreens="false" android:xlargeScreens="false" android:largeScreens="false" android:anyDensity="false" />
<manifest..>